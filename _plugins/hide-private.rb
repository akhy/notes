Jekyll::Hooks.register %i[notes], :pre_render do |doc, payload|
    if ENV['JEKYLL_ENV'] == 'production'
        doc.data["title"] = '404' unless doc.data["public"]
        doc.content = 'Not Found' unless doc.data["public"]
    else
        doc.data["title"] += ' (private)' unless doc.data["public"]
    end
end
