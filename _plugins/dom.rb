require 'nokogiri'

Jekyll::Hooks.register %i[pages documents posts], :post_render do |doc|
  dom = Nokogiri::HTML(doc.output)

  # from minima 3.x
  dom.css('p.feed-subscribe').remove
  dom.css('div.social-links').remove

  doc.output = dom.inner_html
end
