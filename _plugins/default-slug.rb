Jekyll::Hooks.register %i[notes], :post_init do |note|
    note.data["slug"] ||= note.basename
        .sub(/^[0-9]+/, '') # strip zettel ID
        .sub(/\..+$/, '')   # strip file extension
        .strip.downcase
        .gsub("'s", 's')
        .gsub(/[^a-z0-9]/i, '-') # replace non alphanumeric with dash
end
