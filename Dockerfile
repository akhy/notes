FROM ruby:3.1.0

# https://stackoverflow.com/a/70096420
WORKDIR /app
COPY Gemfile Gemfile.lock ./
COPY ./vendor/ ./vendor/
RUN bundle config set --local path 'vendor/ruby' && bundle install
COPY . ./

EXPOSE 8080
CMD ["bundle", "exec", "jekyll", "serve", "--host", "0.0.0.0", "--port", "8080"]
